Test TechEngines

Creare un'applicazione web che permetta di giocare a Memory.

Il Memory (https://it.wikipedia.org/wiki/Memory) è un gioco il cui obbiettivo è trovare le carte uguali 
da un insieme di carte coperte scegliendole 2 per volta.

L'app dovrà:
- permettere la creazione di una nuova partita
- visualizzare su di una griglia le carte (coperte o scoperte)
- permettere di scoprire 2 carte: nel caso siano uguali lasciarle scoperte, altrimenti, passato 5 secondi,
 riportarle nella condizione di coperte
- verificare che la partita sia finita e visualizzare il numero di mosse

Le carte possono essere immagini o anche semplicemente sfondi colorati.
La griglia dovrà contenere 12 carte.

E' possibile usare qualsiasi tecnologia/libreria ritenuta idonea.

Modalità di consegna
 - link github (preferibile)
 - invio file zip 

I seguenti punti saranno considerati un plus:
 - applicazione sviluppata in React.
 - scrittura di test

