import { SET_CARDS, FLIP_CARD, UNFLIP_CARD, LOCK_CARDS, UNLOCK_CARDS } from '../actions';
import _ from 'lodash';

export default function (state = {}, action) {
    let newState = {...state};

    switch (action.type) {
        case SET_CARDS:
            newState.list = _.mapKeys(action.cards, 'id');
            return newState;
        case FLIP_CARD:
            let flipCardList = {...newState.list};
            flipCardList[action.cardId].flipped = true;
            newState.list = flipCardList;
            return newState;
        case UNFLIP_CARD:
            let unFlipCardList = {...newState.list};
            unFlipCardList[action.cardIds[0]].flipped = false;
            unFlipCardList[action.cardIds[1]].flipped = false;
            newState.list = unFlipCardList;
            return newState;
        case LOCK_CARDS:
            newState.lock = true;
            return newState;
        case UNLOCK_CARDS:
            newState.lock = false;
            return newState;
        default:
            return state;
    }
}