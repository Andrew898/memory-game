import { combineReducers } from 'redux';
import CardReducer from './reducer_cards';
import MovementsReducer from './reducer_movs';

const rootReducer = combineReducers({
  cards: CardReducer,
  movements: MovementsReducer
});

export default rootReducer;
