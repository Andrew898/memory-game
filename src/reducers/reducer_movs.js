import { INCREASE_MOVS } from '../actions/';

export default function(state = 0, action) {

    switch (action.type) {
        case INCREASE_MOVS:
            return state + 1;
        default:
            return state;
    }
}