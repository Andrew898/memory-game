export const SET_CARDS = 'set_cards';
export const FLIP_CARD = 'flip_cards';
export const UNFLIP_CARD = 'unflip_cards';
export const INCREASE_MOVS = 'increase_movs';
export const LOCK_CARDS = 'lock_cards';
export const UNLOCK_CARDS = 'unlock_cards';

export function setCards(cards) {

    return {
        type: SET_CARDS,
        cards: cards
    }
}

export function flipCard(cardId) {

    return {
        type: FLIP_CARD,
        cardId: cardId
    }
}

export function unFlipCards(cardIds) {

    return {
        type: UNFLIP_CARD,
        cardIds: cardIds
    }
}

export function increaseMovs() {
    return {
        type: INCREASE_MOVS
    }
}


export function lockCards() {
    return {
        type: LOCK_CARDS
    }
}

export function unLockCards() {
    return {
        type: UNLOCK_CARDS
    }
}
