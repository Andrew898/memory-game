import React, { Component } from 'react';
import CardList from './card_list/card_list';
import './App.css';


class App extends Component {


    render() {
        return (
            <CardList />
        );
    }
}

export default App;
