import React, { Component } from 'react';
import Card from '../card/card';
import { setCards, unFlipCards, lockCards, unLockCards }  from '../actions';
import { connect } from 'react-redux';
import _ from 'lodash';
import './card_list.css';

const CARD_IDS = ['amadeus', "pippobaudo", "pippofranco", "papi", "mazza", "pedro"];
const CARD_NUMBER = 12;
const UNFLIP_TIMEOUT = 5 * 1000;

class CardList extends Component {


    constructor(props) {
        super(props);
        this.state = {
            win: false
        }
    }

    componentWillMount() {
        let mixedCards = this.randomizeCards(CARD_NUMBER);

        let cards = [];
        for (var i = 0; i < mixedCards.length; i++) {
            cards.push({subject: mixedCards[i], id: i, flipped: false, matched: false});
        }

        this.props.setCards(cards);
    }

    componentWillReceiveProps(nextProps) {
        let flippedNotAlreadyMatched = _.filter(nextProps.cards, (card) => {
            return card.flipped && !card.matched;
        });

        if (flippedNotAlreadyMatched.length === 2) {
            nextProps.lockCards();
            if (flippedNotAlreadyMatched[0].subject === flippedNotAlreadyMatched[1].subject) {
                this.matchCards(flippedNotAlreadyMatched[0], flippedNotAlreadyMatched[1]);
                this.checkEndGame();
                nextProps.unLockCards();
            } else {
                setTimeout(function () {
                    nextProps.unFlipCards([flippedNotAlreadyMatched[0].id, flippedNotAlreadyMatched[1].id]);
                    nextProps.unLockCards();
                }, UNFLIP_TIMEOUT);
            }

        }
    }

    matchCards(firstCard, secondCard) {
        firstCard.matched = true;
        secondCard.matched = true;
    }

    checkEndGame() {
        let matchedCards = _.filter(this.props.cards, (card) => {
            return card.matched;
        });

        if (matchedCards.length === CARD_NUMBER) {
            this.winGame();
        }
    }

    winGame() {
        this.setState({win: true});
    }


    randomizeCards(cardsNumber) {

        let cards = [];

        for (var i = 0; i < CARD_IDS.length; i++) {
            for (var j = 0; j < 2; j++) {
                cards.push(CARD_IDS[i]);
            }
        }

        return _.shuffle(cards);
    }

    renderCards() {

        if (!this.props.cards) {
            return (<div></div>);
        }

        let cardList = [];
        let cardIds = Object.keys(this.props.cards);

        for (var i = 0; i < cardIds.length; i++) {
            cardList.push(<Card cardSubject={this.props.cards[i].subject} cardId={this.props.cards[i].id}
                                flipped={this.props.cards[i].flipped} matched={this.props.cards[i].matched}
                                key={i}></Card>);

            if ((i + 1) % 4 === 0) {
                cardList.push(<br key={"br-" + i}/>);
            }
        }

        return cardList;

    }

    render() {



        const cards = this.renderCards();

        return (
            <div className="card-list">
                <div className={`win-alert ${this.state.win ? '' : 'hidden'}`}>
                    <h2>Complimenti, hai terminato il gioco in {this.props.movsNumbers} mosse!</h2>
                </div>
                {cards}
                <p style={{marginTop: '20px'}}>Mosse: <b>{this.props.movsNumbers}</b></p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        cards: state.cards.list,
        movsNumbers: state.movements
    }
}

export default connect(mapStateToProps, {setCards, unFlipCards, lockCards, unLockCards })(CardList);
