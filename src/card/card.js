import React, { Component } from 'react';
import { connect } from 'react-redux';
import { flipCard, increaseMovs } from '../actions';
import './card.css';

export class Card extends Component {

    flipCard() {
        if(!this.props.flipped && !this.props.lock) {
            this.props.flipCard(this.props.cardId);
            this.props.increaseMovs();
        }
    }

    render() {
        const cardClass = `card ${this.props.flipped ? '' : 'back'} ${this.props.matched ? 'matched' : ''}`;

        return (
            <div className={cardClass} onClick={this.flipCard.bind(this)} id={this.props.cardSubject}></div>
        );
    }
}

function mapStateToProps(state) {
    return {
        lock: state.cards.lock
    }
}

export default connect(mapStateToProps, { flipCard, increaseMovs })(Card);
